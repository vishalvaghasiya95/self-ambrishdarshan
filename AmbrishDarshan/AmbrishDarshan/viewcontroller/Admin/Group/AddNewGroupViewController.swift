//
//  AddNewGroupViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 13/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit

class AddNewGroupViewController: UIViewController, UtilityDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var groupTableview: UITableView!
    
    @IBOutlet weak var viewGroupName: UIView!
    @IBOutlet weak var txt_GroupName: UITextField!
    
    var groupListArray:NSArray = NSArray()
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.utility.delegate = self
        groupTableview.delegate = self
        groupTableview.dataSource = self
        Utility.addShadow(view: viewGroupName)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getGroupData()
    }
    
    func getGroupData(){
        Utility.showProgress("")
        let headers = [ "Content-Type": "application/json" ]
        let url = "\(URL_GET_GROUPLIST)"
        var request =  URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        utility.Get_GroupList(request: request, apiType: .GroupList)
    }
    
    //MARK: Button Click Events
    @IBAction func menuButtonClick(_ sender: Any) {
        let mainwindow = UIApplication.shared.windows
        let main = mainwindow[0]
        let transition = CATransition()
        transition.duration = 0.5
        transition.subtype = kCATruncationNone//kCATransitionFromRight
        main.layer.add(transition, forKey: kCATransition)
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func addGroupButtonClick(_ sender: UIButton) {
        Utility.showProgress("Loading...")
        let url = URL(string: URL_CREATE_GROUP)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let parameters = [
            "name":txt_GroupName.text!
        ];
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
            print(error)
        }
        print(parameters)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        utility.CreateGroup(request:urlRequest , apiType:.CreateGroup)
    }
    
    //MARK: Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewGroupTableViewCell") as! NewGroupTableViewCell
        let dic:NSDictionary = groupListArray[indexPath.row] as! NSDictionary
        cell.groupNameLabel.text = dic.value(forKey: "name") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let dic:NSDictionary = self.groupListArray[indexPath.row] as! NSDictionary
        let EditAction = UIContextualAction(style: .normal, title:  "EDIT", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            let alertController = UIAlertController(title: "Update group Name", message: "", preferredStyle: UIAlertControllerStyle.alert)
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Enter Group name"
                textField.text = dic.value(forKey: "name") as? String
            }
            let updateAction = UIAlertAction(title: "Update", style: UIAlertActionStyle.default, handler: { alert -> Void in
                let groupTextField = alertController.textFields![0] as UITextField
                self.editGroupNameApiCall(id: dic.value(forKey: "id") as! String, name: groupTextField.text!)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
                (action : UIAlertAction!) -> Void in })
            
            alertController.addAction(updateAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            success(true)
        })
        
        // Write action code for the Flag
        let DeleteAction = UIContextualAction(style: .normal, title:  "DELETE", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            let id:String = dic.value(forKey: "id") as! String
            self.deleteGroupApiCall(id: id)
            success(true)
        })
        DeleteAction.backgroundColor = .red
        
        return UISwipeActionsConfiguration(actions: [DeleteAction, EditAction])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "GroupMemberListViewController") as! GroupMemberListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteGroupApiCall(id: String){
        Utility.showProgress("Loading...")
        let fullURL = URL_DELETE_GROUP + "group_id="+id
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        var request =  URLRequest(url: URL(string: fullURL)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        utility.DeleteGroup(request: request, apiType: .DeleteGroup)
    }
    
    func editGroupNameApiCall(id: String, name:String){
        Utility.showProgress("Loading...")
        let fullURL = URL_EDIT_GROUP + "update_id="+id
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        var request =  URLRequest(url: URL(string: fullURL)!)
        request.httpMethod = "POST"
        let parameters = [
            "get_updated_name":name
        ];
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
            print(error)
        }
        request.allHTTPHeaderFields = headers
        utility.EditGroup(request: request, apiType: .EditGroup)
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .GroupList:
                groupListArray = data!
                self.groupTableview.reloadData()
                break
            case .CreateGroup:
                getGroupData()
                self.groupTableview.reloadData()
                break
            case .DeleteGroup:
                getGroupData()
                self.groupTableview.reloadData()
                break
            case .EditGroup:
                getGroupData()
                self.groupTableview.reloadData()
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("", message: error, viewController: self)
        }
    }
    
}

