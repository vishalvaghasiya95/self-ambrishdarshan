//
//  HomeViewController.swift
//  AmbrishDarshan
//
//  Created by vishal on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//
import UIKit
import LGSideMenuController
import MWPhotoBrowser
import Contacts
import ContactsUI
import MessageUI
class GroupMemberListViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UtilityDelegate , UITextFieldDelegate , CNContactViewControllerDelegate , MFMessageComposeViewControllerDelegate , MWPhotoBrowserDelegate{
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var userTableview: UITableView!
    
    var isSelected:Bool = Bool()
    
    var utility: Utility = Utility()
    var userList:NSArray = NSArray()
    var filterUserList:NSArray = NSArray()
    var temp:NSArray = NSArray()
    var cityList:NSArray = NSArray()
    var arrSelectedCity = [String]()
    var selectCityID:NSMutableString = NSMutableString()
    var comfirmData:NSString = NSString()
    var selectedCityID = [Int]();
    
    var refreshControl: UIRefreshControl!
    
    var images : [MWPhoto] = []
    var browser : MWPhotoBrowser = MWPhotoBrowser()
    var selectedImage:UIImageView = UIImageView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utility.addShadow(view: searchView)
        self.userTableview.delegate = self
        self.userTableview.dataSource = self
        
        self.utility.delegate = self
        self.searchTextField.delegate = self
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        userTableview.addSubview(refreshControl)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        getUserData()
    }
    
    func getUserData(){
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        let url = "\(URL_USER_LIST)?city=1,2,3,4,5,6,7"
        var request =  URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        utility.Get_UserList(request: request, apiType: .UserList)
    }
    
    @objc func refresh(){
        self.getUserData()
    }
    
    //MARK:- Button Click Events
    @IBAction func menuButtonClick(_ sender: Any) {
        let mainwindow = UIApplication.shared.windows
        let main = mainwindow[0]
        let transition = CATransition()
        transition.duration = 0.5
        transition.subtype = kCATruncationNone//kCATransitionFromRight
        main.layer.add(transition, forKey: kCATransition)
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @objc func numberCLickEvent(sender: UIButton) {
        let temp:NSDictionary = userList[sender.tag] as! NSDictionary
        let alert = UIAlertController(title: "Choose Option", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
            self.call(phoneNumber: temp.value(forKey: "devotee_mobile_no") as! String)
        }))
        alert.addAction(UIAlertAction(title: "Message", style: .default, handler: { (action) in
            self.message(phoneNumber: temp.value(forKey: "devotee_mobile_no") as! String)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func call(phoneNumber:String){
        if let url = URL(string: "tel://\(phoneNumber)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    func message(phoneNumber:String){
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Message Body"
            controller.recipients = [phoneNumber]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    //MARK:- Compose Message Delegate Method.
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func imageTapped(sender:UIButton){
        let temp:NSDictionary = userList[sender.tag] as! NSDictionary
        
        let url = URL(string: "http://hddevelopers.in/ambrishdarshannew/Admin/images/\(temp.value(forKey: "devotee_image") ?? "")")
        self.images.removeAll()
        self.images.append(MWPhoto(url: url!))
        
        self.browser.delegate = self
        browser.displayActionButton = false
        browser.displayNavArrows = false
        browser.displaySelectionButtons = false
        browser.alwaysShowControls = false
        browser.zoomPhotosToFill = true
        browser.enableGrid = false
        browser.startOnGrid = false
        browser.enableSwipeToDismiss = false
        browser.autoPlayOnAppear = false
        //browser.setCurrentPhotoIndex(1)
        browser.reloadData();
        let nc : UINavigationController = UINavigationController(rootViewController: self.browser)
        //self.browser.setCurrentPhotoIndex(UInt(1))
        self.present(nc, animated: true, completion: nil)
        
    }
    
    //MARK:- MWPhotoBrowserDelegate
    func numberOfPhotos(in photoBrowser: MWPhotoBrowser!) -> UInt {
        return UInt(self.images.count)
    }
    
    func photoBrowser(_ photoBrowser: MWPhotoBrowser!, photoAt index: UInt) -> MWPhotoProtocol! {
        if(index < UInt(self.images.count)) {
            return self.images[Int(index)]
        }
        return nil
    }
    
    func photoBrowser(_ photoBrowser: MWPhotoBrowser!, isPhotoSelectedAt index: UInt) -> Bool {
        return false
    }
    
    func photoBrowserDidFinishModalPresentation(_ photoBrowser: MWPhotoBrowser!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Textfield Delegate Method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        let string1 = string
        let string2 = self.searchTextField.text
        var finalString = ""
        if string.count > 0 { // if it was not delete character
            finalString = string2! + string1
        }
        else if (string2?.count)! > 0 {
            finalString = String.init(string2!.dropLast())
        }
        filteredArray(searchString: finalString as NSString)
        return true
    }
    
    func filteredArray(searchString:NSString){
            filterUserList = temp
            if searchString == "" {
                self.getUserData()
            }
            else{
                let predicate = NSPredicate(format: "devotee_name contains[c] %@ OR devotee_mobile_no contains[c] %@",searchString , searchString)
                userList = filterUserList.filtered(using: predicate) as NSArray
            }
        self.userTableview.reloadData()
    }
    
    func cityFilter(searchString:NSString){
        if searchString == "" {
            self.getUserData()
        }
        else{
            let headers = [ "Content-Type": "application/json" ]
            let url = "\(URL_USER_LIST)?city=\(searchString)"
            var request =  URLRequest(url: URL(string: url)!)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            utility.Get_UserList(request: request, apiType: .UserList)
        }
        self.userTableview.reloadData()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            let farray = selectedCityID.filter {$0 != 0}
            self.selectCityID = ""
            if farray.count > 0 {
                for i in farray {
                    selectCityID.append("\(i),")
                }
                var data:NSString = NSString()
                data = selectCityID.mutableCopy() as! NSString
                comfirmData = data.substring(with: NSMakeRange((data.length - 1) - (data.length - 1), data.length - 1)) as NSString
                self.cityFilter(searchString: comfirmData)
            }
            else{
                selectCityID = ""
                self.cityFilter(searchString: "")
            }
        }
        return true
    }
    
    
    //MARK: Tableview Delegate Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
            let data:NSDictionary = userList[indexPath.row] as! NSDictionary
            
            cell.lbl_name.text = data.value(forKey: "devotee_name") as? String
            
            cell.lbl_MobileNumber.tag = indexPath.row
            //            cell.lbl_MobileNumber.addTarget(self, action: #selector(call(sender:)), for: .touchUpInside)
            //            cell.lbl_MobileNumber.setTitle("Mo.\(data.value(forKey: "devotee_mobile_no") ?? "")", for: .normal)
            cell.lbl_MobileNumber.text = "MO:-\(data.value(forKey: "devotee_mobile_no") ?? "")"
            if data.value(forKey: "devotee_image") != nil {
                cell.profileImageView.sd_setImage(with: URL(string: "http://hddevelopers.in/ambrishdarshannew/Admin/images/\(data.value(forKey: "devotee_image") ?? "")"))
            }
            cell.callButton.tag = indexPath.row
            cell.callButton.addTarget(self, action: #selector(numberCLickEvent(sender:)), for: .touchUpInside)
            
            cell.profileImageTapButton.tag = indexPath.row
            cell.profileImageTapButton.addTarget(self, action: #selector(imageTapped(sender:)), for: .touchUpInside)
            
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let data:NSDictionary = userList[indexPath.row] as! NSDictionary
            let userName:String = data.value(forKey: "devotee_name") as! String
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsViewController") as! UserDetailsViewController
            vc.userName = userName
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 80.0
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            let dic:NSDictionary = userList[indexPath.row] as! NSDictionary
            let id:String = dic.value(forKey: "devotee_name") as! String
//            self.deleteUserApiCall(name: id)
        }
    }
    
    func deleteUserApiCall(name: String){
        Utility.showProgress("Loading...")
        let fullURL = URL_DELETE_USER + "name="+name
        let urlNew:String = fullURL.replacingOccurrences(of: " ", with: "%20")
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        var request =  URLRequest(url: URL(string: urlNew)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.DeleteAdmin(request: request, apiType: .DeleteUser)
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .UserList:
                self.refreshControl.endRefreshing()
                userList = []
                userList = data!
                filterUserList = userList
                self.temp = userList
                self.userTableview.reloadData()
                break
            case .DeleteUser:
                getUserData()
                break
            default:
                break
            }
        }
        else{
            userList = []
            self.userTableview.reloadData()
            self.refreshControl.endRefreshing()
            Utility.showAlert("", message: error, viewController: self)
        }
    }
}


