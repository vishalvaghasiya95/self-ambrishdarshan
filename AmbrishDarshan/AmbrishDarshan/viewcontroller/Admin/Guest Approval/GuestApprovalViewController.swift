//
//  GuestApprovalViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 13/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit

class GuestApprovalViewController: UIViewController, UtilityDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewReuest: UIView!
    @IBOutlet weak var viewApproved: UIView!
    @IBOutlet weak var requestMainView: UIView!
    @IBOutlet weak var approvedMainView: UIView!
    
    @IBOutlet weak var requestableview: UITableView!
    @IBOutlet weak var approvedTablelview: UITableView!
    
    @IBOutlet weak var requestButton: UIButton!
    @IBOutlet weak var approveButton: UIButton!
    
    var guestReuqestListArray:NSArray = NSArray()
    var approveReuqestListArray:NSArray = NSArray()
    
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.utility.delegate = self
        
        viewReuest.backgroundColor = BORDER_COLOR
        viewApproved.backgroundColor = WHITE_COLOR
        
        requestMainView.isHidden = false
        approvedMainView.isHidden = true
        
        requestableview.delegate = self
        requestableview.dataSource = self
        
        approvedTablelview.delegate = self
        approvedTablelview.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getGuestRequestList()
        getApproveRequestList()
    }
    
    func getGuestRequestList(){
        Utility.showProgress("")
        let headers = [ "Content-Type": "application/json" ]
        var request =  URLRequest(url: URL(string: URL_GUEST_REQUEST)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.Get_SuggestionList(request: request, apiType: .GuestRequest)
    }
    
    func getApproveRequestList(){
        Utility.showProgress("")
        let headers = [ "Content-Type": "application/json" ]
        var request =  URLRequest(url: URL(string: URL_GUEST_APPROVE)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.Get_GuestApproveList(request: request, apiType: .GuestApprove)
    }
    
    //MARK: Button Click Events
    @IBAction func menuButtonClick(_ sender: Any) {
        let mainwindow = UIApplication.shared.windows
        let main = mainwindow[0]
        let transition = CATransition()
        transition.duration = 0.5
        transition.subtype = kCATruncationNone//kCATransitionFromRight
        main.layer.add(transition, forKey: kCATransition)
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func tab_RequestButtonClick(_ sender: UIButton) {
        viewReuest.backgroundColor = BORDER_COLOR
        viewApproved.backgroundColor = WHITE_COLOR
        
        requestMainView.isHidden = false
        approvedMainView.isHidden = true
    }
    
    @IBAction func tab_ApprovadButtonClick(_ sender: UIButton) {
        viewReuest.backgroundColor = WHITE_COLOR
        viewApproved.backgroundColor = BORDER_COLOR
        
        requestMainView.isHidden = true
        approvedMainView.isHidden = false
        
    }
    
    @objc func approveButtonClick(sender: UIButton){
        let data:NSDictionary = guestReuqestListArray[sender.tag] as! NSDictionary
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        let id:String = data.value(forKey: "id") as! String
        let url = URL_APPROVE_REQUEST + "id=\(id)"
        var request =  URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.ApproveRequest(request: request, apiType: .ApproveRequest)
    }
    
    @objc func deleteButtonClick(sender: UIButton){
        let data:NSDictionary = guestReuqestListArray[sender.tag] as! NSDictionary
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        let id:String = data.value(forKey: "id") as! String
        let url = URL_DELETE_REQUEST + "id=\(id)"
        var request =  URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.DeleteRequest(request: request, apiType: .DeleteRequest)
    }
    
    @objc func deleteApproveGuestButtonClick(sender: UIButton){
        let data:NSDictionary = approveReuqestListArray[sender.tag] as! NSDictionary
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        let id:String = data.value(forKey: "id") as! String
        let url = URL_DELETE_REQUEST + "id=\(id)"
        var request =  URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.DeleteRequest(request: request, apiType: .DeleteRequest)
    }
    
    //MARK: Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == requestableview {
            return guestReuqestListArray.count
        }
        else{
            return approveReuqestListArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == requestableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReuqestTableViewCell") as! ReuqestTableViewCell
            let data:NSDictionary = guestReuqestListArray[indexPath.row] as! NSDictionary
            cell.lbl_Name.text = data.value(forKey: "name") as? String
            cell.lbl_MobileNumber.text = data.value(forKey: "mobile") as? String
            cell.lbl_Date.text = data.value(forKey: "date") as? String
            cell.lbl_Pradesh.text = data.value(forKey: "pradesh") as? String
            cell.lbl_Village.text = data.value(forKey: "village") as? String
            cell.lbl_City.text = data.value(forKey: "city") as? String

            cell.approveButton.layer.cornerRadius = 5
            cell.deleteButton.layer.cornerRadius = 5
            
            Utility.imageViewBorder(image: cell.userImage)
            
            cell.approveButton.tag = indexPath.row
            cell.deleteButton.tag = indexPath.row
            cell.approveButton.addTarget(self, action: #selector(approveButtonClick(sender:)), for: .touchUpInside)
            cell.deleteButton.addTarget(self, action: #selector(deleteButtonClick(sender:)), for: .touchUpInside)
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ApprovedTableViewCell") as! ApprovedTableViewCell
            let data:NSDictionary = approveReuqestListArray[indexPath.row] as! NSDictionary
            cell.lbl_Name.text = data.value(forKey: "name") as? String
            cell.lbl_MobileNumber.text = data.value(forKey: "mobile") as? String
            cell.lbl_Date.text = data.value(forKey: "date") as? String
            cell.lbl_Pradesh.text = data.value(forKey: "pradesh") as? String
            cell.lbl_Village.text = data.value(forKey: "village") as? String
            cell.lbl_City.text = data.value(forKey: "city") as? String
            Utility.imageViewBorder(image: cell.userImage)
            
            cell.deleteButton.layer.cornerRadius = 5
            
            
            cell.deleteButton.tag = indexPath.row
            cell.deleteButton.addTarget(self, action: #selector(deleteApproveGuestButtonClick(sender:)), for: .touchUpInside)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == requestableview {
            return 210
        }
        else{
            return 210
        }
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .GuestRequest:
                guestReuqestListArray = data!
                requestButton.setTitle("REQUEST (\(guestReuqestListArray.count))", for: .normal)
                self.requestableview.reloadData()
                break
            case .GuestApprove:
                approveReuqestListArray = data!
                approveButton.setTitle("APPROVED (\(approveReuqestListArray.count))", for: .normal)
                self.approvedTablelview.reloadData()
                break
            case .ApproveRequest:
                getGuestRequestList()
                getApproveRequestList()
                break
            case .DeleteRequest:
                getGuestRequestList()
                getApproveRequestList()
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("", message: error, viewController: self)
        }
    }
}

