//
//  ApprovedTableViewCell.swift
//  AmbrishDarshan
//
//  Created by MAC on 13/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit

class ApprovedTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_MobileNumber: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Pradesh: UILabel!
    @IBOutlet weak var lbl_City: UILabel!
    @IBOutlet weak var lbl_Village: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var userImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
