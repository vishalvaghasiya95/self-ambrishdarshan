//
//  AddAdminViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 13/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit

class AddAdminViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UtilityDelegate {
    
    @IBOutlet weak var addButtonView: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewMobileNumber: UIView!
    
    @IBOutlet weak var txt_Name: UITextField!
    @IBOutlet weak var txt_MobileNumber: UITextField!
    
    @IBOutlet weak var adminTableview: UITableView!
    
    var adminList:NSMutableArray = NSMutableArray()
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.utility.delegate = self
        Utility.addShadow(view: viewMobileNumber)
        Utility.addShadow(view: viewName)
        Utility.addShadow(view: addButtonView)
        
        adminTableview.delegate = self
        adminTableview.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getAdminAPI()
    }
    
    func getAdminAPI(){
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        var request =  URLRequest(url: URL(string: URL_GET_ADMIN)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.GetAdminList(request: request, apiType: .GetAdmin)
    }
    
    
    //MARK: Button Click Events
    @IBAction func menuButtonClick(_ sender: Any) {
        let mainwindow = UIApplication.shared.windows
        let main = mainwindow[0]
        let transition = CATransition()
        transition.duration = 0.5
        transition.subtype = kCATruncationNone//kCATransitionFromRight
        main.layer.add(transition, forKey: kCATransition)
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func addButtonClick(_ sender: UIButton) {
        Utility.showProgress("Loading...")
        let url = URL(string: URL_ADD_ADMIN)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let parameters = [
            "name":txt_Name.text!,
            "mobile":txt_MobileNumber.text!
        ];
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
            print(error)
        }
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        utility.AddAdmin(request:urlRequest , apiType:.AddAdmin)
    }
    
    //MARK: Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return adminList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddAdminTableViewCell") as! AddAdminTableViewCell
        let dic:NSDictionary = adminList[indexPath.row] as! NSDictionary
        cell.lbl_Name.text = dic.value(forKey: "name") as? String
        cell.lbl_MobileNumber.text = dic.value(forKey: "mobile") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            let dic:NSDictionary = adminList[indexPath.row] as! NSDictionary
            let id:String = dic.value(forKey: "id") as! String
            self.deleteAdminApiCall(id: id)
        }
    }
    
    func deleteAdminApiCall(id: String){
        Utility.showProgress("Loading...")
        let fullURL = URL_DELETE_ADMIN + "id="+id
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        var request =  URLRequest(url: URL(string: fullURL)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.DeleteAdmin(request: request, apiType: .DeleteAdmin)
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .AddAdmin:
                self.getAdminAPI()
                break
            case .GetAdmin:
                adminList = data?.mutableCopy() as! NSMutableArray
                self.adminTableview.reloadData()
                break
            case .DeleteAdmin:
                 self.getAdminAPI()
                 self.adminTableview.reloadData()
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("", message: error, viewController: self)
        }
    }
    
}

