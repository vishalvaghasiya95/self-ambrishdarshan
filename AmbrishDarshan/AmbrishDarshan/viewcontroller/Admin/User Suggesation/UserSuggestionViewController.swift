//
//  UserSuggestionViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 13/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit

class UserSuggestionViewController: UIViewController, UtilityDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var suggetionTableview: UITableView!
    
    var contentHeight:CGFloat = CGFloat()
    var suggestionListArray:NSArray = NSArray()
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.utility.delegate = self
        
        suggetionTableview.delegate = self
        suggetionTableview.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getSuggestionList()
    }
    
    func getSuggestionList(){
        Utility.showProgress("")
        let headers = [ "Content-Type": "application/json" ]
        var request =  URLRequest(url: URL(string: URL_SUGGESTION_LIST)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.Get_GuestRequestList(request: request, apiType: .SuggestionList)
    }
    
    //MARK: Button Click Events
    @IBAction func menuButtonClick(_ sender: Any) {
        let mainwindow = UIApplication.shared.windows
        let main = mainwindow[0]
        let transition = CATransition()
        transition.duration = 0.5
        transition.subtype = kCATruncationNone//kCATransitionFromRight
        main.layer.add(transition, forKey: kCATransition)
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    //MARK: Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggestionListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserSuggestionTableViewCell") as! UserSuggestionTableViewCell
        let data:NSDictionary = suggestionListArray[indexPath.row] as! NSDictionary
        cell.lbl_Name.text = data.value(forKey: "name") as? String
        cell.lbl_MobileNumber.text = data.value(forKey: "mobile") as? String
        cell.lbl_Date.text = data.value(forKey: "date") as? String
        cell.lbl_Title.text = data.value(forKey: "title") as? String
        cell.txtView_Message.text = data.value(forKey: "message") as? String
        contentHeight = cell.txtView_Message.contentSize.height
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140 + contentHeight
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .SuggestionList:
                suggestionListArray = data!
                self.suggetionTableview.reloadData()
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("", message: error, viewController: self)
        }
    }
}

