//
//  UserSuggestionTableViewCell.swift
//  AmbrishDarshan
//
//  Created by MAC on 13/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit

class UserSuggestionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_MobileNumber: UILabel!
    @IBOutlet weak var txtView_Message: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
