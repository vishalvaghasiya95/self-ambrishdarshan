//
//  AddDevoteeViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 04/11/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit
import CZPicker
import Alamofire
class AddDevoteeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CZPickerViewDelegate, CZPickerViewDataSource, UtilityDelegate {

    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewId: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewWorkAddress: UIView!
    @IBOutlet weak var viewPradesh: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewOccupation: UIView!
    @IBOutlet weak var viewMobileNumber: UIView!
    @IBOutlet weak var viewDOB: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewBloodGroup: UIView!
//    @IBOutlet weak var viewAdmin: UIView!
//    @IBOutlet weak var viewUser: UIView!
//    @IBOutlet weak var viewIncome: UIView!
    @IBOutlet weak var viewMonthly: UIView!
    @IBOutlet weak var viewYearly: UIView!
    @IBOutlet weak var viewNotes: UIView!
//    @IBOutlet weak var viewPicker: UIView!

    @IBOutlet weak var imgDevotee: UIImageView!
    @IBOutlet weak var txtDevoteeID: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var txtWorkAddress: UITextView!
    @IBOutlet weak var txtPradesh: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtOccupation: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtBloodGroup: UITextField!
    @IBOutlet weak var txtMonthlyIncome: UITextField!
    @IBOutlet weak var txtYearlyIncome: UITextField!
    @IBOutlet weak var txtNotes: UITextView!

    @IBOutlet weak var imgAdmin: UIImageView!
    @IBOutlet weak var imgUser: UIImageView!

    @IBOutlet weak var lblAddressPlaceHolder: UILabel!
    @IBOutlet weak var lblWorkAddressPlaceHolder: UILabel!
    @IBOutlet weak var lblNotesPlaceHolder: UILabel!

    @IBOutlet weak var adminSelectImage: UIImageView!
    @IBOutlet weak var userSelectImage: UIImageView!
    
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var utility: Utility = Utility()
    
    var cityList:[String] = []
    var bloodList:[String] = []
    var isCity: Bool = Bool()
    var userRole:String = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.utility.delegate = self
        Utility.addShadow(view: viewProfile)
        Utility.addShadow(view: viewId)
        Utility.addShadow(view: viewName)
        Utility.addShadow(view: viewAddress)
        Utility.addShadow(view: viewWorkAddress)
        Utility.addShadow(view: viewPradesh)
        Utility.addShadow(view: viewCity)
        Utility.addShadow(view: viewOccupation)
        Utility.addShadow(view: viewMobileNumber)
        Utility.addShadow(view: viewDOB)
        Utility.addShadow(view: viewEmail)
        Utility.addShadow(view: viewBloodGroup)
//        Utility.addShadow(view: viewAdmin)
//        Utility.addShadow(view: viewUser)
        Utility.addShadow(view: viewMonthly)
        Utility.addShadow(view: viewYearly)
        Utility.addShadow(view: viewNotes)
        Utility.addImageShadow(image: imgDevotee)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(OpenImagePicker(Gesture:)))
        tap.numberOfTapsRequired = 1
        self.imgDevotee.addGestureRecognizer(tap)
        
        self.cityList = ["Rajkot","Jamnagar","Jasdan", "Jasapar", "Savar kundala", "Gondal", "Morabi"]
        self.bloodList = ["A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func OpenImagePicker(Gesture:UITapGestureRecognizer){
        imageActionSheet()
    }
    
    func imageActionSheet() {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if(UIImagePickerController.isSourceTypeAvailable(.camera)) {
                self.camera()
            }
            else {
                Utility.showAlert(self.title, message: "Camera not available on your device", viewController: self)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    // MARK: - ImagePicker Delegate Method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgDevotee.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Button Click Events
    @IBAction func adminButtonClick(_ sender: UIButton) {
        adminSelectImage.image = UIImage(named: "radio-on")
        userSelectImage.image = UIImage(named: "radio-off")
        userRole = "admin"
    }
    
    @IBAction func userButtonClick(_ sender: UIButton) {
        adminSelectImage.image = UIImage(named: "radio-off")
        userSelectImage.image = UIImage(named: "radio-on")
        userRole = "user"
    }
    
    @IBAction func selectCityButtonClick(_ sender: UIButton) {
        isCity = true
        let picker = CZPickerView(headerTitle: "Select City", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
        picker?.delegate = self
        picker?.dataSource = self
        picker?.needFooterView = true
        picker?.show()
    }
    
    @IBAction func selectBloodGroup(_ sender: UIButton) {
        isCity = false
        let picker = CZPickerView(headerTitle: "Select Boold Group", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
        picker?.delegate = self
        picker?.dataSource = self
        picker?.needFooterView = true
        picker?.show()
    }
   
    @IBAction func selectDatePickerButtonClick(_ sender: UIButton) {
        self.datePickerView.isHidden = false
        self.datePicker.addTarget(self, action: #selector(dateValueChanged), for: .valueChanged)
    }
    
    @objc func dateValueChanged(){
    }
    
    @IBAction func datePickerDoneButtonClick(_ sender: UIButton) {
        self.datePickerView.isHidden = true
        txtDOB.text = Date.getFormattedDate(string: "\(datePicker.date)")
    }
    
    @IBAction func addDevoteeButtonClick(_ sender: UIButton) {
        Utility.showProgress("Loading...")
        let url = URL(string: URL_ADDDEVOTEE)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        let parameters = [
            "get_name":txtName.text,
            "get_id":txtDevoteeID.text,
            "get_address":txtAddress.text,
            "get_work_address":txtWorkAddress.text,
            "get_city":txtCity.text,
            "get_mobile":txtMobileNumber.text,
            "get_occupation":txtOccupation.text,
            "get_dob":txtDOB.text,
            "get_email":txtEmailAddress.text,
            "get_blood":txtBloodGroup.text,
            "get_role":userRole,
            "get_notes":txtNotes.text,
            "income_monthly":txtMonthlyIncome.text,
            "income_yearly":txtYearlyIncome.text,
        ];
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
            print(error)
        }
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let imgData = UIImageJPEGRepresentation(self.imgDevotee.image!, 0.2)!
        self.utility.AddDevotee(request: urlRequest, imgData: imgData, fileName: txtName.text!, parameters: parameters as! [String : String], apiType: .Registration)
    }
    
    //MARK:- CZPicker view Delegate Method
    func numberOfRows(in pickerView: CZPickerView!) -> Int {
        if isCity {
            return cityList.count
        } else{
            return bloodList.count
        }
    }
    
    func czpickerView(_ pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        if isCity {
            return "\(cityList[row])";
        } else {
            return "\(bloodList[row])";
        }
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemAtRow row: Int){
        if isCity {
            self.txtCity.text = cityList[row]
        } else {
            self.txtBloodGroup.text = bloodList[row]
        }
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .AddDevotee:
                
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("", message: error, viewController: self)
        }
    }
}
