//
//  NewAddDevoteeViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 13/12/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit
import CZPicker
class NewAddDevoteeViewController: UIViewController, UtilityDelegate, CZPickerViewDelegate, CZPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewId: UIView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewWorkAddress: UIView!
    @IBOutlet weak var viewPradesh: UIView!
    @IBOutlet weak var viewCity: UIView!
    @IBOutlet weak var viewOccupation: UIView!
    @IBOutlet weak var viewMobileNumber: UIView!
    @IBOutlet weak var viewDOB: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewBloodGroup: UIView!
    @IBOutlet weak var viewMonthly: UIView!
    @IBOutlet weak var viewYearly: UIView!
    @IBOutlet weak var viewNotes: UIView!
    
    @IBOutlet weak var Img_AdminSelect: UIImageView!
    @IBOutlet weak var Img_UserSelect: UIImageView!
    
    @IBOutlet weak var TXT_DevoteeID: UITextField!
    @IBOutlet weak var TXT_Name: UITextField!
    @IBOutlet weak var TXT_Address: UITextView!
    @IBOutlet weak var TXT_WorkAddress: UITextView!
    @IBOutlet weak var TXT_Pradesh: UITextField!
    @IBOutlet weak var TXT_Occupation: UITextField!
    @IBOutlet weak var TXT_City: UITextField!
    @IBOutlet weak var TXT_Mobile: UITextField!
    @IBOutlet weak var TXT_DOB: UITextField!
    @IBOutlet weak var TXT_EmailAddress: UITextField!
    @IBOutlet weak var TXT_BloodGroup: UITextField!
    @IBOutlet weak var TXT_Monthaly: UITextField!
    @IBOutlet weak var TXT_Yearly: UITextField!
    @IBOutlet weak var TXT_Notes: UITextView!
    
    @IBOutlet weak var birthPickerView: UIView!
    @IBOutlet weak var birthDatePicker: UIDatePicker!
    
    @IBOutlet weak var insertDevoteeButton: UIButton!
    
    var utility: Utility = Utility()
    var cityList:[String] = []
    var bloodList:[String] = []
    var isCity: Bool = Bool()
    var userRole:String = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.utility.delegate = self
        Utility.addShadow(view: viewProfile)
        Utility.addShadow(view: viewId)
        Utility.addShadow(view: viewName)
        Utility.addShadow(view: viewAddress)
        Utility.addShadow(view: viewWorkAddress)
        Utility.addShadow(view: viewPradesh)
        Utility.addShadow(view: viewCity)
        Utility.addShadow(view: viewOccupation)
        Utility.addShadow(view: viewMobileNumber)
        Utility.addShadow(view: viewDOB)
        Utility.addShadow(view: viewEmail)
        Utility.addShadow(view: viewBloodGroup)
        //        Utility.addShadow(view: viewAdmin)
        //        Utility.addShadow(view: viewUser)
        Utility.addShadow(view: viewMonthly)
        Utility.addShadow(view: viewYearly)
        Utility.addShadow(view: viewNotes)
        Utility.addImageShadow(image: userImage)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(OpenImagePicker(Gesture:)))
        tap.numberOfTapsRequired = 1
        self.userImage.addGestureRecognizer(tap)
        
        insertDevoteeButton.addTarget(self, action: #selector(ApiCall(sender:)), for: .touchUpInside)
        
        self.cityList = ["Rajkot","Jamnagar","Jasdan", "Jasapar", "Savar kundala", "Gondal", "Morabi"]
        self.bloodList = ["A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-"]
        
        TXT_Monthaly.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        TXT_Yearly.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TextField Delegate Method
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == TXT_Monthaly {
//            let M = (TXT_Monthaly.text! as NSString).doubleValue / Double(12)
            let Y = (TXT_Monthaly.text! as NSString).doubleValue * Double(12)
            let round = String(format: "%.2f", Y)
            TXT_Yearly.text = "\(round)"
        } else {
            let M = (TXT_Yearly.text! as NSString).doubleValue / Double(12)
//            let Y = (TXT_Monthaly.text! as NSString).doubleValue * Double(12)
            let round = String(format: "%.2f", M)
            TXT_Monthaly.text = "\(round)"
        }
    }
    
    //MARK: Button Click Events
    @IBAction func menuButtonCLick(_ sender: UIButton) {
        let mainwindow = UIApplication.shared.windows
        let main = mainwindow[0]
        let transition = CATransition()
        transition.duration = 0.5
        transition.subtype = kCATruncationNone//kCATransitionFromRight
        main.layer.add(transition, forKey: kCATransition)
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func adminButtonClick(_ sender: UIButton) {
        Img_AdminSelect.image = UIImage(named: "radio-on")
        Img_UserSelect.image = UIImage(named: "radio-off")
        userRole = "admin"
    }
    
    @IBAction func userButtonClick(_ sender: UIButton) {
        Img_AdminSelect.image = UIImage(named: "radio-off")
        Img_UserSelect.image = UIImage(named: "radio-on")
        userRole = "user"
    }

    @IBAction func selectCityButtonClick(_ sender: UIButton) {
        isCity = true
        let picker = CZPickerView(headerTitle: "Select City", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
        picker?.delegate = self
        picker?.dataSource = self
        picker?.needFooterView = true
        picker?.show()
    }
    
    @IBAction func selectBloodGroupButtonClick(_ sender: UIButton) {
        isCity = false
        let picker = CZPickerView(headerTitle: "Select Boold Group", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
        picker?.delegate = self
        picker?.dataSource = self
        picker?.needFooterView = true
        picker?.show()
    }
    
    @IBAction func selectDOBBUttonClick(_ sender: UIButton) {
        self.birthPickerView.isHidden = false
        self.birthDatePicker.addTarget(self, action: #selector(dateValueChanged), for: .valueChanged)
    }

    @objc func dateValueChanged(){
    }
    
    @IBAction func pickerDoneButtonClick(_ sender: UIButton) {
        self.birthPickerView.isHidden = true
        TXT_DOB.text = Date.getFormattedDate(string: "\(birthDatePicker.date)")
    }
    
    //MARK:- CZPicker view Delegate Method
    func numberOfRows(in pickerView: CZPickerView!) -> Int {
        if isCity {
            return cityList.count
        } else{
            return bloodList.count
        }
    }
    
    func czpickerView(_ pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        if isCity {
            return "\(cityList[row])";
        } else {
            return "\(bloodList[row])";
        }
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemAtRow row: Int){
        if isCity {
            self.TXT_City.text = cityList[row]
        } else {
            self.TXT_BloodGroup.text = bloodList[row]
        }
    }
    
    @objc func OpenImagePicker(Gesture:UITapGestureRecognizer){
        imageActionSheet()
    }

    func imageActionSheet() {

        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if(UIImagePickerController.isSourceTypeAvailable(.camera)) {
                self.camera()
            }
            else {
                Utility.showAlert(self.title, message: "Camera not available on your device", viewController: self)
            }
        }))

        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }

    func camera() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
    }

    func photoLibrary() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }

    // MARK: - ImagePicker Delegate Method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.userImage.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func ApiCall(sender:UIButton){
        Utility.showProgress("Loading...")
        let url = URL(string: URL_ADDDEVOTEE)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"


        let parameters = [
            "get_name": TXT_Name.text!,
            "get_id":TXT_DevoteeID.text!,
            "get_city":TXT_City.text!,
            "get_mobile":TXT_Mobile.text!,
            "get_occupation":TXT_Occupation.text!,
            "get_dob":TXT_DOB.text!,
            "get_address":TXT_Address.text!,
            "get_work_address":TXT_WorkAddress.text!,
            "get_email":TXT_EmailAddress.text!,
            "get_blood":TXT_BloodGroup.text!,
            "get_role":userRole,
            "get_notes":TXT_Notes.text!,
            "income_monthly":TXT_Monthaly.text!,
            "income_yearly":TXT_Yearly.text!,
            ] as [String : Any];
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
            print(error)
        }
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let imgData = UIImageJPEGRepresentation(self.userImage.image!, 0.2)!
        self.utility.AddDevotee(request: urlRequest, imgData: imgData, fileName: TXT_Name.text!, parameters: parameters as! [String : String], apiType: .AddDevotee)
    }
    
//    @IBAction func addButtonClick(_ sender: UIButton) {
//        Utility.showProgress("Loading...")
//        let url = URL(string: URL_ADDDEVOTEE)!
//        var urlRequest = URLRequest(url: url)
//        urlRequest.httpMethod = "POST"
//        let parameters = [
//            "get_name":TXT_Name.text,
//            "get_id":TXT_DevoteeID.text,
//            "get_address":TXT_Address.text,
//            "get_work_address":TXT_WorkAddress.text,
//            "get_city":TXT_City.text,
//            "get_mobile":TXT_Mobile.text,
//            "get_occupation":TXT_Occupation.text,
//            "get_dob":TXT_DOB.text,
//            "get_email":TXT_EmailAddress.text,
//            "get_blood":TXT_BloodGroup.text,
//            "get_role":userRole,
//            "get_notes":TXT_Notes.text,
//            "income_monthly":TXT_Monthaly.text,
//            "income_yearly":TXT_Yearly.text,
//            ];
//        do {
//            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
//        } catch {
//            print(error)
//        }
//        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        let imgData = UIImageJPEGRepresentation(self.userimage.image!, 0.2)!
//        self.utility.AddDevotee(request: urlRequest, imgData: imgData, fileName: TXT_Name.text!, parameters: parameters as! [String : String], apiType: .Registration)
//    }
    
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .AddDevotee:
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "AdminHomeViewController") as! AdminHomeViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("", message: error, viewController: self)
        }
    }
}
