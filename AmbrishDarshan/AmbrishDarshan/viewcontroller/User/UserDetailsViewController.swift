//
//  UserDetailsViewController.swift
//  AmbrishDarshan
//
//  Created by vishal on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit
import AddressBook
import MWPhotoBrowser
import Contacts
import ContactsUI
import MessageUI
class UserDetailsViewController: UIViewController ,UtilityDelegate , MWPhotoBrowserDelegate , CNContactViewControllerDelegate , MFMessageComposeViewControllerDelegate{
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var lbl_UserName: UILabel!
    @IBOutlet weak var lbl_UserID: UILabel!
    @IBOutlet weak var lbl_UserAddress: UILabel!
    @IBOutlet weak var lbl_UserWorkAddress: UILabel!
    @IBOutlet weak var lbl_UserType: UILabel!
    @IBOutlet weak var lbl_UserCity: UILabel!
    @IBOutlet weak var lbl_UserWork: UILabel!
    @IBOutlet weak var lbl_UserMobileNumber: UILabel!
    @IBOutlet weak var lbl_UserDOB: UILabel!
    @IBOutlet weak var lbl_UserEmail: UILabel!
    @IBOutlet weak var lbl_UserBloodGroup: UILabel!
    
    var userName:String = ""
    let utility = Utility()
    var images : [MWPhoto] = []
    var browser : MWPhotoBrowser = MWPhotoBrowser()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestur = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        tapGestur.numberOfTapsRequired = 1
        self.profileImageView.isUserInteractionEnabled = true
        self.profileImageView.addGestureRecognizer(tapGestur)
        
        let calltapGestur = UITapGestureRecognizer(target: self, action: #selector(callOptions))
        calltapGestur.numberOfTapsRequired = 1
        self.lbl_UserMobileNumber.isUserInteractionEnabled = true
        self.lbl_UserMobileNumber.addGestureRecognizer(calltapGestur)
        
        let longPressGestur = UILongPressGestureRecognizer(target: self, action: #selector(copyAddress(sender:)))
        self.lbl_UserAddress.isUserInteractionEnabled = true
        self.lbl_UserAddress.addGestureRecognizer(longPressGestur)
        
        self.utility.delegate = self
        Utility.imageBorder(view: profileView)
        
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        let url = "\(URL_GETUSER_FULLDATA)?name_=\(userName)"
        let newURL = url.replacingOccurrences(of: " ", with: "%20")
        var request =  URLRequest(url: URL(string:newURL)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.Get_UserDetails(request: request, apiType: .UserDetails)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    @objc func imageTapped(){
        self.images.append(MWPhoto(image: profileImageView.image))
        self.browser.delegate = self
        browser.displayActionButton = false
        browser.displayNavArrows = false
        browser.displaySelectionButtons = false
        browser.alwaysShowControls = false
        browser.zoomPhotosToFill = true
        browser.enableGrid = false
        browser.startOnGrid = false
        browser.enableSwipeToDismiss = false
        browser.autoPlayOnAppear = false
        browser.setCurrentPhotoIndex(0)
        
        let nc : UINavigationController = UINavigationController(rootViewController: self.browser)
        self.browser.setCurrentPhotoIndex(UInt(0))
        self.present(nc, animated: true, completion: nil)
    }
    
    //MARK:- Button Click Events
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveContactButtonClick(_ sender: UIButton) {
        let newContact = CNMutableContact()
        newContact.phoneNumbers.append(CNLabeledValue(label: lbl_UserName.text, value: CNPhoneNumber(stringValue: lbl_UserMobileNumber.text!)))
        let contactVC = CNContactViewController(forUnknownContact: newContact)
        contactVC.contactStore = CNContactStore()
        contactVC.delegate = self
        contactVC.allowsActions = false
        //        let navigationController = UINavigationController(rootViewController: contactVC) //For presenting the vc you have to make it navigation controller otherwise it will not work, if you already have navigatiation controllerjust push it you dont have to make it a navigation controller
        self.navigationController?.pushViewController(contactVC, animated: true)
        //        self.present(navigationController, animated: true, completion: nil)
    }
    
    @objc func copyAddress(sender:UILongPressGestureRecognizer){
        let pasteboard = UIPasteboard.general
        pasteboard.string = self.lbl_UserAddress.text
        Utility.showAlert("Copy", message: "address copied.", viewController: self)
    }
    
    @objc func callOptions(){
        let alert = UIAlertController(title: "Choose Option", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
            self.call(phoneNumber: self.lbl_UserMobileNumber.text!)
        }))
        alert.addAction(UIAlertAction(title: "Message", style: .default, handler: { (action) in
            self.message(phoneNumber: self.lbl_UserMobileNumber.text!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func call(phoneNumber:String){
        if let url = URL(string: "tel://\(phoneNumber)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    func message(phoneNumber:String){
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Message Body"
            controller.recipients = [phoneNumber]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    //MARK:- Compose Message Delegate Method.
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK:- MWPhotoBrowserDelegate
    func numberOfPhotos(in photoBrowser: MWPhotoBrowser!) -> UInt {
        return UInt(self.images.count)
    }
    
    func photoBrowser(_ photoBrowser: MWPhotoBrowser!, photoAt index: UInt) -> MWPhotoProtocol! {
        if(index < UInt(self.images.count)) {
            return self.images[Int(index)]
        }
        return nil
    }
    
    func photoBrowser(_ photoBrowser: MWPhotoBrowser!, isPhotoSelectedAt index: UInt) -> Bool {
        return false
    }
    
    func photoBrowserDidFinishModalPresentation(_ photoBrowser: MWPhotoBrowser!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Contact Save Delegate Method
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .UserDetails:
                let data = AppData.sharedInstance.userDetailsDic
                self.lbl_UserName.text = data.value(forKey: "devotee_name") as? String
                self.lbl_UserType.text = data.value(forKey: "devotee_pradesh") as? String
                self.lbl_UserID.text = "ID: \(data.value(forKey: "devotee_unique_id") as? String ?? "")"
                self.lbl_UserDOB.text = data.value(forKey: "devotee_dob") as? String
                let cityID = data.value(forKey: "devotee_city") as? String
                if cityID == "1"{
                    self.lbl_UserCity.text = "Rajkot"
                }
                else if  cityID == "2"{
                    self.lbl_UserCity.text = "Jamnagar"
                }
                else if cityID == "3"{
                    self.lbl_UserCity.text = "Jasdan"
                }
                else if cityID == "4"{
                    self.lbl_UserCity.text = "Jasapar"
                }
                else if cityID == "5"{
                    self.lbl_UserCity.text = "Savar kundla"
                }
                else if cityID == "6"{
                    self.lbl_UserCity.text = "Gondal"
                }
                else if cityID == "7"{
                    self.lbl_UserCity.text = "Morbi"
                }
                self.lbl_UserWork.text = data.value(forKey: "devotee_occupation") as? String
                if data.value(forKey: "devotee_email") as! String != "" {
                    self.lbl_UserEmail.textColor = UIColor.black
                    self.lbl_UserEmail.text = data.value(forKey: "devotee_email") as? String
                }
                self.lbl_UserAddress.text = data.value(forKey: "devotee_address") as? String
                self.lbl_UserAddress.sizeToFit()
                if data.value(forKey: "work_address") as! String != "" {
                    self.lbl_UserWorkAddress.textColor = UIColor.black
                    self.lbl_UserWorkAddress.text = data.value(forKey: "work_address") as? String
                }
                if data.value(forKey: "devotee_bloodgroup") as! String != "" {
                    self.lbl_UserBloodGroup.text = data.value(forKey: "devotee_bloodgroup") as? String
                }
                self.lbl_UserMobileNumber.text = data.value(forKey: "devotee_mobile_no") as? String
                if data.value(forKey: "devotee_image") != nil {
                    profileImageView.sd_setImage(with: URL(string: "http://hddevelopers.in/ambrishdarshannew/Admin/images/\(data.value(forKey: "devotee_image") ?? "")"))
                }
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("", message: error, viewController: self)
        }
    }
}

