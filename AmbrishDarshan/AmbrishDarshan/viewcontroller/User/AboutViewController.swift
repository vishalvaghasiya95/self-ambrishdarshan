//
//  AboutViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Button Click Events
    @IBAction func menuButtonCLick(_ sender: UIButton) {
        let mainwindow = UIApplication.shared.windows
        let main = mainwindow[0]
        let transition = CATransition()
        transition.duration = 0.5
        transition.subtype = kCATruncationNone//kCATransitionFromRight
        main.layer.add(transition, forKey: kCATransition)
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

