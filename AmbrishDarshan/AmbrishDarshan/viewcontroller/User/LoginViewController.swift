//
//  LoginViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit
import Alamofire
class LoginViewController: UIViewController, UtilityDelegate , UITextFieldDelegate {

    @IBOutlet weak var txt_MobileTextField: UITextField!
    @IBOutlet weak var mobileView: UIView!
    
    // PopupView
    @IBOutlet weak var popupview: UIView!
    @IBOutlet weak var view_TextView: UIView!
    @IBOutlet weak var txt_comfirmationCodeTextfield: UITextField!
    @IBOutlet weak var view_SubmitButtonView: UIView!
    
    var loginData:NSArray = NSArray()
    var utility: Utility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.utility.delegate = self
        self.txt_MobileTextField.delegate = self
        
        Utility.addShadow(view: mobileView)
        Utility.addShadow(view: view_TextView)
        Utility.addShadow(view: view_SubmitButtonView)
        self.popupview.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        var request =  URLRequest(url: URL(string: URL_LOGIN)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.UserLogin(request: request, apiType: .Login)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Mobile Number Validation.
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^[6-9][0-9]{9}$";
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    //MARK:- Button Click Events
    @IBAction func guestButtonClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController")as! RegistrationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func submitButtonClick(_ sender: UIButton) {
        if self.txt_comfirmationCodeTextfield.text?.count == 4 {
            let someString = self.txt_comfirmationCodeTextfield.text
            if let myInteger = Int(someString!) {
                let OTP = NSNumber(value:myInteger)
                if AppData.sharedInstance.OTP == OTP {
                    self.popupview.isHidden = true
                        if UserDefaults.standard.value(forKey: "UserRole") as! String == "admin" {
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
                            let vc = storyboard.instantiateViewController(withIdentifier: "AdminHomeViewController") as! AdminHomeViewController
                            navigationController.setViewControllers([(vc)], animated: false)
                            let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                            
                            mainViewController.rootViewController = navigationController
                            mainViewController.setup(type: UInt(11))
                            self.navigationController?.pushViewController(mainViewController, animated: true)
                        }
                        else{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
                            let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            navigationController.setViewControllers([(vc)], animated: false)
                            let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                            
                            mainViewController.rootViewController = navigationController
                            mainViewController.setup(type: UInt(11))
                            self.navigationController?.pushViewController(mainViewController, animated: true)
                        }
                }
                else{
                    Utility.showAlert("", message: "Please Enter Valid OTP", viewController: self)
                }
            }
        }
        else{
            Utility.showAlert("", message: "Please Enter OTP", viewController: self)
        }
    }
    
    //MARK:- Textfield Delegate Method
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let value = "\(textField.text!)\(string)"
        for i in self.loginData {
            if validate(value: value){
                let data:NSDictionary = i as! NSDictionary
                if data.value(forKey: "devotee_mobile_no") as! String  == value{
                    textField.resignFirstResponder()
                    UserDefaults.standard.set(value, forKey: "UserMobileNumber")
                    let role = data.value(forKey: "role")
                    UserDefaults.standard.set(role, forKey: "UserRole")
                    Utility.showProgress("Loading...")
                    let headers = [ "Content-Type": "application/json" ]
                    let url = "\(URL_SMS)?mobile=\(value)"
                    var request =  URLRequest(url: URL(string: url)!)
                    request.httpMethod = "POST"
                    request.allHTTPHeaderFields = headers
                    utility.SendSMS(request: request, apiType: .Sms)
                }
            }
            else{
                if value.count == 10 {
                    Utility.showAlert("Message", message: "Please Enter valid Number.", viewController: self)
                }
            }
        }
        
        return true
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .Login:
                self.loginData = data!
                break
            case .Sms:
                self.popupview.isHidden = false
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("", message: error, viewController: self)
        }
    }

}
