//
//  ContactUsViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit
import CZPicker
class ContactUsViewController: UIViewController , UtilityDelegate , CZPickerViewDelegate, CZPickerViewDataSource{
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var suggestionView: UIView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var suggestionLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    
    var utility = Utility()
    var suggession:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utility.addShadow(view: nameView)
        Utility.addShadow(view: mobileView)
        Utility.addShadow(view: suggestionView)
        Utility.addShadow(view: messageView)
        Utility.addShadow(view: buttonView)
        
        utility.delegate = self
        
        self.nameTextField.text = AppData.sharedInstance.userProfileDic.value(forKey: "devotee_name") as? String
        self.mobileTextField.text = UserDefaults.standard.value(forKey: "UserMobileNumber") as? String
        //        self.messageTextView.toolbarPlaceholder = "Enter Youre Message"
        self.suggession = ["Sugession","Query","Other"];
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Button Click Events
    @IBAction func menuButtonClick(_ sender: UIButton) {
        let mainwindow = UIApplication.shared.windows
        let main = mainwindow[0]
        let transition = CATransition()
        transition.duration = 0.5
        transition.subtype = kCATruncationNone//kCATransitionFromRight
        main.layer.add(transition, forKey: kCATransition)
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func suggestionButtonClick(_ sender: UIButton) {
        let picker = CZPickerView(headerTitle: "Select", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
        picker?.delegate = self
        picker?.dataSource = self
        picker?.needFooterView = true
        picker?.show()
    }
    
    @IBAction func submitButtonClick(_ sender: UIButton) {
        if suggestionLabel.text != "" && messageTextView.text != "" && suggestionLabel.text != "" && mobileTextField.text != ""{
            Utility.showProgress("Loading...")
            let url = URL(string: URL_SUGGESSION)!
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "POST"
            let parameters = [
                "name":self.nameTextField.text,
                "mobile":self.mobileTextField.text,
                "type":self.suggestionLabel.text,
                "message":self.messageTextView.text
            ];
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                print(error)
            }
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            self.utility.User_Sugession(request: urlRequest, parameters: parameters as! [String : String], apiType: .Sugession)
        }
        else{
            Utility.showAlert("Alert", message: "All field required!", viewController: self)
        }
    }
    
    //MARK:- CZPicker view Delegate Method
    func numberOfRows(in pickerView: CZPickerView!) -> Int {
        return suggession.count
    }
    
    func czpickerView(_ pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        return "\(suggession[row])";
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemAtRow row: Int){
        self.suggestionLabel.text = suggession[row]
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .Sugession:
                Utility.showAlert("Message", message: "Query Success!", viewController: self)
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("", message: error, viewController: self)
        }
    }
}

