//
//  RegistrationViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit
import Alamofire
class RegistrationViewController: UIViewController , UtilityDelegate , UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var registerButtonView: UIView!
    @IBOutlet weak var pradeshView: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var villageView: UIView!
    
    @IBOutlet weak var txt_NameTextField: UITextField!
    @IBOutlet weak var txt_MobileTextField: UITextField!
    @IBOutlet weak var img_ProfileImageView: UIImageView!
    @IBOutlet weak var pradeshTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var villageTextField: UITextField!
    
    var utility: Utility = Utility()
    
    var loginData:NSArray = NSArray()
    var numberExistes:Bool = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.utility.delegate = self
        
        Utility.addShadow(view: nameView)
        Utility.addShadow(view: mobileView)
        Utility.addShadow(view: registerButtonView)
        Utility.imageBorder(view: imageView)
        Utility.addShadow(view: pradeshView)
        Utility.addShadow(view: cityView)
        Utility.addShadow(view: villageView)
        
        self.img_ProfileImageView.isUserInteractionEnabled = true
        let tapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapClicked))
        tapGesture.numberOfTapsRequired = 1;
        self.img_ProfileImageView.addGestureRecognizer(tapGesture)
        
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        var request =  URLRequest(url: URL(string: URL_LOGIN)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.UserLogin(request: request, apiType: .Login)
        self.checkNumber()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Button Click Events
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registrationButtonClick(_ sender: UIButton) {
        self.checkNumber()
        if self.img_ProfileImageView.image != nil && txt_MobileTextField.text != "" && txt_NameTextField.text != "" {
            if numberExistes == true {
                Utility.showAlert("Alert", message: "Mobile number already exists.", viewController: self)
            }
            else{
                Utility.showProgress("Loading...")
                let url = URL(string: URL_REGISTRATION)!
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = "POST"
                let parameters = [
                    "get_name":txt_NameTextField.text,
                    "get_mobile":txt_MobileTextField.text,
                    "get_city":cityTextField.text,
                    "get_pradesh":pradeshTextField.text,
                    "get_village":villageTextField.text
                ];
                do {
                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                } catch {
                    print(error)
                }
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let imgData = UIImageJPEGRepresentation(self.img_ProfileImageView.image!, 0.2)!
                self.utility.User_Registration(request: urlRequest, imgData: imgData, fileName: txt_NameTextField.text!, parameters: parameters as! [String : String], apiType: .Registration)
            }
        }
        else{
            Utility.showAlert("Alert", message: "All field required!", viewController: self)
        }
    }
    
    func checkNumber(){
        for i in self.loginData {
            let data:NSDictionary = i as! NSDictionary
            if data.value(forKey: "devotee_mobile_no") as? String  == txt_MobileTextField.text {
                numberExistes = true
            }
        }
    }
    
    //MARK:- Tap Gesture Click
    @objc func imageTapClicked(){
        imageActionSheet()
    }
    
    func imageActionSheet() {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            if(UIImagePickerController.isSourceTypeAvailable(.camera)) {
                self.camera()
            }
            else {
                Utility.showAlert(self.title, message: "Camera not available on your device", viewController: self)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    func photoLibrary() {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    // MARK: - ImagePicker Delegate Method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.img_ProfileImageView.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func validate(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$";
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .Registration:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
                break
            case .Login:
                self.loginData = data!
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("", message: error, viewController: self)
        }
    }
}

