//
//  HomeViewController.swift
//  AmbrishDarshan
//
//  Created by vishal on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//
import UIKit
import LGSideMenuController
import MWPhotoBrowser
import Contacts
import ContactsUI
import MessageUI
class HomeViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , UtilityDelegate , UITextFieldDelegate , CNContactViewControllerDelegate , MFMessageComposeViewControllerDelegate , MWPhotoBrowserDelegate{
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var userTableview: UITableView!
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var selectAllImageview: UIImageView!
    @IBOutlet weak var filterTableview: UITableView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var okView: UIView!
    
    var isSelected:Bool = Bool()
    
    var utility: Utility = Utility()
    var userList:NSArray = NSArray()
    var filterUserList:NSArray = NSArray()
    var temp:NSArray = NSArray()
    var cityList:NSArray = NSArray()
    var arrSelectedCity = [String]()
    var selectCityID:NSMutableString = NSMutableString()
    var comfirmData:NSString = NSString()
    var selectedCityID = [Int]();
    
    var refreshControl: UIRefreshControl!
    
    var images : [MWPhoto] = []
    var browser : MWPhotoBrowser = MWPhotoBrowser()
    var selectedImage:UIImageView = UIImageView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utility.addShadow(view: searchView)
        Utility.addShadow(view: cancelView)
        Utility.addShadow(view: okView)
        self.cornerView.layer.cornerRadius = 10.0
        self.cornerView.clipsToBounds = true
        
        self.userTableview.delegate = self
        self.userTableview.dataSource = self
        self.filterTableview.delegate = self
        self.filterTableview.dataSource = self
        
        self.utility.delegate = self
        self.searchTextField.delegate = self
        
        Utility.showProgress("Loading...")
        let headers1 = [ "Content-Type": "application/json" ]
        var request1 =  URLRequest(url: URL(string: URL_CITY_LIST)!)
        request1.httpMethod = "GET"
        request1.allHTTPHeaderFields = headers1
        utility.Get_CityList(request: request1, apiType: .CityList)
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        userTableview.addSubview(refreshControl)
        
        self.filterView.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func getUserData(){
        let headers = [ "Content-Type": "application/json" ]
        let url = "\(URL_USER_LIST)?city=1,2,3,4,5,6,7"
        var request =  URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        utility.Get_UserList(request: request, apiType: .UserList)
    }
    
    func blanckGetUserData(){
        let headers = [ "Content-Type": "application/json" ]
        let url = "\(URL_USER_LIST)?city=0"
        var request =  URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        utility.Get_UserList(request: request, apiType: .UserList)
    }
    
    @objc func refresh(){
        //        self.getUserData()
        let farray = selectedCityID.filter {$0 != 0}
        self.selectCityID = ""
        if farray.count > 0 {
            for i in farray {
                selectCityID.append("\(i),")
            }
            var data:NSString = NSString()
            data = selectCityID.mutableCopy() as! NSString
            comfirmData = data.substring(with: NSMakeRange((data.length - 1) - (data.length - 1), data.length - 1)) as NSString
            self.cityFilter(searchString: comfirmData)
        }
        else{
            selectCityID = ""
            self.cityFilter(searchString: "")
        }
    }
    
    //MARK:- Button Click Events
    @IBAction func menuButtonClick(_ sender: Any) {
        let mainwindow = UIApplication.shared.windows
        let main = mainwindow[0]
        let transition = CATransition()
        transition.duration = 0.5
        transition.subtype = kCATruncationNone//kCATransitionFromRight
        main.layer.add(transition, forKey: kCATransition)
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func filterButtonClick(_ sender: UIButton) {
        self.filterView.isHidden = false
    }
    
    @IBAction func selectAllButtonClick(_ sender: UIButton) {
        if isSelected == false{
            self.selectAllImageview.image = UIImage(named: "check")
            self.selectAllCity()
            isSelected = true
        }
        else{
            self.selectAllImageview.image = UIImage(named: "uncheck")
            self.reset()
            isSelected = false
        }
    }
    
    @IBAction func filterCancelButtonClick(_ sender: UIButton) {
        self.filterView.isHidden = true
    }
    
    @IBAction func filterOkButtonClick(_ sender: UIButton) {
        self.filterView.isHidden = true
        let farray = selectedCityID.filter {$0 != 0}
        self.selectCityID = ""
        if farray.count > 0 {
            for i in farray {
                selectCityID.append("\(i),")
            }
            var data:NSString = NSString()
            data = selectCityID.mutableCopy() as! NSString
            comfirmData = data.substring(with: NSMakeRange((data.length - 1) - (data.length - 1), data.length - 1)) as NSString
            self.cityFilter(searchString: comfirmData)
        }
        else{
            selectCityID = ""
            self.cityFilter(searchString: "")
        }
    }
    
    func selectAllCity() {
        arrSelectedCity.removeAll()
        selectedCityID.removeAll()
        var count = 1
        for _ in cityList {
            arrSelectedCity.append("1")
            selectedCityID.append(count)
            count = count + 1
        }
        self.filterTableview.reloadData()
    }
    
    func reset() {
        arrSelectedCity.removeAll()
        selectedCityID.removeAll()
        for _ in 0..<cityList.count {
            self.arrSelectedCity.append("0")
            self.selectedCityID.append(0)
        }
        self.filterTableview.reloadData()
    }
    
    @objc func selectCity(sender:UIButton){
        let dic:NSDictionary = cityList[sender.tag] as! NSDictionary
        if arrSelectedCity[sender.tag] == "0" {
            arrSelectedCity[sender.tag] = "1"
            let id:Int = Int(dic.value(forKey: "id") as! String)!
            selectedCityID[sender.tag] = id
        } else {
            arrSelectedCity[sender.tag] = "0"
            selectedCityID[sender.tag] = 0
        }
        self.filterTableview.reloadData()
    }
    
    @objc func numberCLickEvent(sender: UIButton) {
        let temp:NSDictionary = userList[sender.tag] as! NSDictionary
        let alert = UIAlertController(title: "Choose Option", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Call", style: .default, handler: { (action) in
            self.call(phoneNumber: temp.value(forKey: "devotee_mobile_no") as! String)
        }))
        alert.addAction(UIAlertAction(title: "Message", style: .default, handler: { (action) in
            self.message(phoneNumber: temp.value(forKey: "devotee_mobile_no") as! String)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func call(phoneNumber:String){
        if let url = URL(string: "tel://\(phoneNumber)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    func message(phoneNumber:String){
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Message Body"
            controller.recipients = [phoneNumber]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    //MARK:- Compose Message Delegate Method.
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func imageTapped(sender:UIButton){
        let temp:NSDictionary = userList[sender.tag] as! NSDictionary
        
        let url = URL(string: "http://hddevelopers.in/ambrishdarshannew/Admin/images/\(temp.value(forKey: "devotee_image") ?? "")")
        //let tempData:NSData = try! NSData(contentsOf: url!)
        //selectedImage.image = UIImage(data: tempData as Data)
        //self.images.append(MWPhoto(image: selectedImage.image))
        self.images.removeAll()
        self.images.append(MWPhoto(url: url!))
        
        self.browser.delegate = self
        browser.displayActionButton = false
        browser.displayNavArrows = false
        browser.displaySelectionButtons = false
        browser.alwaysShowControls = false
        browser.zoomPhotosToFill = true
        browser.enableGrid = false
        browser.startOnGrid = false
        browser.enableSwipeToDismiss = false
        browser.autoPlayOnAppear = false
        //browser.setCurrentPhotoIndex(1)
        browser.reloadData();
        let nc : UINavigationController = UINavigationController(rootViewController: self.browser)
        //self.browser.setCurrentPhotoIndex(UInt(1))
        self.present(nc, animated: true, completion: nil)
        
    }
    
    //MARK:- MWPhotoBrowserDelegate
    func numberOfPhotos(in photoBrowser: MWPhotoBrowser!) -> UInt {
        return UInt(self.images.count)
    }
    
    func photoBrowser(_ photoBrowser: MWPhotoBrowser!, photoAt index: UInt) -> MWPhotoProtocol! {
        if(index < UInt(self.images.count)) {
            return self.images[Int(index)]
        }
        return nil
    }
    
    func photoBrowser(_ photoBrowser: MWPhotoBrowser!, isPhotoSelectedAt index: UInt) -> Bool {
        return false
    }
    
    func photoBrowserDidFinishModalPresentation(_ photoBrowser: MWPhotoBrowser!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Textfield Delegate Method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        let string1 = string
        let string2 = self.searchTextField.text
        var finalString = ""
        if string.count > 0 { // if it was not delete character
            finalString = string2! + string1
        }
        else if (string2?.count)! > 0 {
            finalString = String.init(string2!.dropLast())
        }
        filteredArray(searchString: finalString as NSString)
        return true
    }
    
    func filteredArray(searchString:NSString){
        if userList.count > 0 {
            filterUserList = temp
            if searchString == "" {
                //            self.getUserData()
                blanckGetUserData()
            }
            else{
                let predicate = NSPredicate(format: "devotee_name contains[c] %@ OR devotee_mobile_no contains[c] %@",searchString , searchString)
                userList = filterUserList.filtered(using: predicate) as NSArray
            }
        }
        else{
            if searchString == "" {
                let farray = selectedCityID.filter {$0 != 0}
                self.selectCityID = ""
                if farray.count > 0 {
                    for i in farray {
                        selectCityID.append("\(i),")
                    }
                    var data:NSString = NSString()
                    data = selectCityID.mutableCopy() as! NSString
                    comfirmData = data.substring(with: NSMakeRange((data.length - 1) - (data.length - 1), data.length - 1)) as NSString
                    self.cityFilter(searchString: comfirmData)
                }
                else{
                    selectCityID = ""
                    self.cityFilter(searchString: "")
                }
            }
        }
        self.userTableview.reloadData()
    }
    
    func cityFilter(searchString:NSString){
        if searchString == "" {
            //            self.getUserData()
            blanckGetUserData()
        }
        else{
            let headers = [ "Content-Type": "application/json" ]
            let url = "\(URL_USER_LIST)?city=\(searchString)"
            var request =  URLRequest(url: URL(string: url)!)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            utility.Get_UserList(request: request, apiType: .UserList)
        }
        self.userTableview.reloadData()
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            let farray = selectedCityID.filter {$0 != 0}
            self.selectCityID = ""
            if farray.count > 0 {
                for i in farray {
                    selectCityID.append("\(i),")
                }
                var data:NSString = NSString()
                data = selectCityID.mutableCopy() as! NSString
                comfirmData = data.substring(with: NSMakeRange((data.length - 1) - (data.length - 1), data.length - 1)) as NSString
                self.cityFilter(searchString: comfirmData)
            }
            else{
                selectCityID = ""
                self.cityFilter(searchString: "")
            }
        }
        return true
    }
    
    //MAARK:- Tableview Delegate Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.userTableview {
            return self.userList.count
        }
        else{
            return cityList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.userTableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
            let data:NSDictionary = userList[indexPath.row] as! NSDictionary
            
            cell.lbl_name.text = data.value(forKey: "devotee_name") as? String
            
            cell.lbl_MobileNumber.tag = indexPath.row
            //            cell.lbl_MobileNumber.addTarget(self, action: #selector(call(sender:)), for: .touchUpInside)
            //            cell.lbl_MobileNumber.setTitle("Mo.\(data.value(forKey: "devotee_mobile_no") ?? "")", for: .normal)
            cell.lbl_MobileNumber.text = "MO:-\(data.value(forKey: "devotee_mobile_no") ?? "")"
            if data.value(forKey: "devotee_image") != nil {
                cell.profileImageView.sd_setImage(with: URL(string: "http://hddevelopers.in/ambrishdarshannew/Admin/images/\(data.value(forKey: "devotee_image") ?? "")"))
            }
            cell.callButton.tag = indexPath.row
            cell.callButton.addTarget(self, action: #selector(numberCLickEvent(sender:)), for: .touchUpInside)
            
            cell.profileImageTapButton.tag = indexPath.row
            cell.profileImageTapButton.addTarget(self, action: #selector(imageTapped(sender:)), for: .touchUpInside)
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell") as! FilterTableViewCell
            let data:NSDictionary = cityList[indexPath.row] as! NSDictionary
            
            cell.selectCityButton.tag = indexPath.row
            cell.selectCityButton.addTarget(self, action: #selector(selectCity(sender:)), for: .touchUpInside)
            if arrSelectedCity[indexPath.row] == "1" {
                cell.selectCityImage.image = UIImage(named: "check")
            } else {
                cell.selectCityImage.image = UIImage(named: "uncheck")
            }
            
            cell.cityTitleLabel.text = data.value(forKey: "city") as? String
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.userTableview == tableView {
            let data:NSDictionary = userList[indexPath.row] as! NSDictionary
            let userName:String = data.value(forKey: "devotee_name") as! String
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsViewController") as! UserDetailsViewController
            vc.userName = userName
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            //            let data:NSDictionary = cityList[indexPath.row] as! NSDictionary
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.userTableview == tableView {
            return 80.0
        }
        else{
            return 40.0
        }
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .CityList:
                self.cityList = data!
                self.reset()
                self.filterTableview.reloadData()
                break
            case .UserList:
                self.refreshControl.endRefreshing()
                userList = []
                userList = data!
                filterUserList = userList
                self.temp = userList
                self.userTableview.reloadData()
                break
            default:
                break
            }
        }
        else{
            userList = []
            self.userTableview.reloadData()
            self.refreshControl.endRefreshing()
            Utility.showAlert("", message: error, viewController: self)
        }
    }
}

