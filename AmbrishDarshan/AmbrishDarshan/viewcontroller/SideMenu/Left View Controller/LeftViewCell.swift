//
//  LeftViewCell.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import LGSideMenuController
class LeftViewCell: UITableViewCell {
    
    @IBOutlet weak var img_IconImageView: UIImageView!
    @IBOutlet weak var lbl_MenuLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
