//
//  LeftViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit
import SDWebImage
class LeftViewController: UIViewController ,UtilityDelegate , UITableViewDelegate , UITableViewDataSource{
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImagebackView: UIView!
    
    @IBOutlet weak var menuTableview: UITableView!
    
    var UserMenuArray:NSMutableArray = NSMutableArray()
    var AdminMenuArray:NSMutableArray = NSMutableArray()
    var UserProfile:String = String()
    
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.utility.delegate = self
        
        self.userImagebackView.layer.cornerRadius = userImagebackView.frame.size.width / 2;
        self.userImagebackView.clipsToBounds = true
        self.userImagebackView.layer.borderWidth = 1.0
        self.userImagebackView.layer.borderColor = UIColor.white.cgColor
        
        Utility.showProgress("Loading...")
        let headers = [ "Content-Type": "application/json" ]
        let url = "\(URL_GET_USERPROFILE)?login_id=\(UserDefaults.standard.value(forKey: "UserMobileNumber") ?? "")"
        var request =  URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        utility.UserProfileData(request: request, apiType: .UserProfile)
        
        UserProfile = UserDefaults.standard.value(forKey: "UserRole") as! String
        
        self.menuTableview.delegate = self
        self.menuTableview.dataSource = self
        
        let userMenu1:[String:String] = ["lable":"List Of Ambrish Devotees", "icon":"menu1"]
        let userMenu2:[String:String] = ["lable":"Notifications", "icon":"menu2"]
        let userMenu3:[String:String] = ["lable":"Contact Us", "icon":"menu8"]
        let userMenu4:[String:String] = ["lable":"Abous Us", "icon":"menu8"]
        let userMenu5:[String:String] = ["lable":"Logout", "icon":"menu10"]
        
        UserMenuArray.addObjects(from: [userMenu1,userMenu2,userMenu3,userMenu4,userMenu5])
        
        let adminMenu1:[String:String] = ["lable":"List Of Ambrish Devotees", "icon":"menu1"]
        let adminMenu2:[String:String] = ["lable":"Notifications", "icon":"menu2"]
        let adminMenu3:[String:String] = ["lable":"Add New Ambrish Devotees", "icon":"menu2"]
        let adminMenu4:[String:String] = ["lable":"Add New Admin", "icon":"menu2"]
        let adminMenu5:[String:String] = ["lable":"Groups", "icon":"menu5"]
        let adminMenu6:[String:String] = ["lable":"Guest Approval", "icon":"menu2"]
        let adminMenu7:[String:String] = ["lable":"User's Suggestion", "icon":"menu2"]
        let adminMenu8:[String:String] = ["lable":"Logout", "icon":"menu10"]
        
        AdminMenuArray.addObjects(from: [adminMenu1,adminMenu2,adminMenu3,adminMenu4,adminMenu5,adminMenu6,adminMenu7,adminMenu8])
        self.menuTableview.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool{
        
        return true
    }
    
    //MARK: Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserProfile == "admin" {
            return AdminMenuArray.count
        }
        else{
            return UserMenuArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewCell") as! LeftViewCell
        if UserProfile == "admin" {
            let data:NSDictionary = AdminMenuArray[indexPath.row] as! NSDictionary
            cell.lbl_MenuLabel.text = data.value(forKey: "lable") as? String
            cell.img_IconImageView.image = UIImage(named: data.value(forKey: "icon") as! String)
        }
        else{
            
            let data:NSDictionary = UserMenuArray[indexPath.row] as! NSDictionary
            cell.lbl_MenuLabel.text = data.value(forKey: "lable") as? String
            cell.img_IconImageView.image = UIImage(named: data.value(forKey: "icon") as! String)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UserProfile == "admin" {
            let data:NSDictionary = AdminMenuArray[indexPath.row] as! NSDictionary
            let label = data.value(forKey: "lable") as? String
            if label == "List Of Ambrish Devotees" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "AdminHomeViewController") as! AdminHomeViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "Notifications" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "Add New Ambrish Devotees" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "NewAddDevoteeViewController") as! NewAddDevoteeViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "Add New Admin" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "AddAdminViewController") as! AddAdminViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "Groups" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "AddNewGroupViewController") as! AddNewGroupViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "Guest Approval" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "GuestApprovalViewController") as! GuestApprovalViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "User's Suggestion" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "UserSuggestionViewController") as! UserSuggestionViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "Logout" {
                UserDefaults.standard.set("", forKey: "UserMobileNumber")
                UserDefaults.standard.set("", forKey: "UserID")
                UserDefaults.standard.set("", forKey: "UserRole")

                UserDefaults.standard.synchronize()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginPageView = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                let nav = UINavigationController(rootViewController: loginPageView)
                nav.isNavigationBarHidden = true
                let appdalegate = UIApplication.shared.delegate as! AppDelegate
                appdalegate.window?.rootViewController = nav
                appdalegate.window?.makeKeyAndVisible()
            }
        }
        else{
            let data:NSDictionary = UserMenuArray[indexPath.row] as! NSDictionary
            let label = data.value(forKey: "lable") as? String
            if label == "List Of Ambrish Devotees" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "Notifications" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "Contact Us" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "Abous Us" {
                let mainViewController = sideMenuController!
                let navigationController = mainViewController.rootViewController as! NavigationController
                let  mybulidingVC = self.storyboard!.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
                navigationController.setViewControllers([mybulidingVC], animated: false)
                mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
            }
            else if label == "Logout" {
                UserDefaults.standard.set("", forKey: "UserMobileNumber")
                UserDefaults.standard.set("", forKey: "UserID")
                UserDefaults.standard.set("", forKey: "UserRole")

                UserDefaults.standard.synchronize()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginPageView = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                let nav = UINavigationController(rootViewController: loginPageView)
                nav.isNavigationBarHidden = true
                let appdalegate = UIApplication.shared.delegate as! AppDelegate
                appdalegate.window?.rootViewController = nav
                appdalegate.window?.makeKeyAndVisible()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .UserProfile:
                let data = AppData.sharedInstance.userProfileDic
                self.userNameLabel.text = data.value(forKey: "devotee_name") as? String
                UserDefaults.standard.set(data.value(forKey: "devotee_unique_id"), forKey: "UserID")
                if data.value(forKey: "devotee_image") != nil {
                    
                }
                else{
                    self.userImageView.image = UIImage(named: "ambrish_bhakt")
                }
                break
            default:
                break
            }
        }
        else{
//            UserDefaults.standard.set("", forKey: "UserMobileNumber")
//            UserDefaults.standard.set("", forKey: "UserID")
//            UserDefaults.standard.synchronize()
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let loginPageView = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//            let nav = UINavigationController(rootViewController: loginPageView)
//            nav.isNavigationBarHidden = true
//            let appdalegate = UIApplication.shared.delegate as! AppDelegate
//            appdalegate.window?.rootViewController = nav
//            appdalegate.window?.makeKeyAndVisible()
        }
    }
    
    
}
