//
//  MainViewController.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import LGSideMenuController
class MainViewController: LGSideMenuController {
    
    private var type: UInt?
    
    func setup(type: UInt) {
        self.type = 2
        if (self.storyboard != nil) {
        }
        else {
            leftViewController = LeftViewController()
            
            leftViewWidth = 300.0;
            leftViewBackgroundImage = UIImage(named: "imageLeft")
            leftViewBackgroundColor = UIColor(red: 0.5, green: 0.65, blue: 0.5, alpha: 0.95)
            rootViewCoverColorForLeftView = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.05)
            
        }
        
        let regularStyle: UIBlurEffectStyle
        
        if #available(iOS 10.0, *) {
            regularStyle = .regular
        }
        else {
            regularStyle = .light
        }
        
        rootViewScaleForLeftView = 0.8
        rootViewCoverBlurEffectForLeftView = UIBlurEffect(style: regularStyle)
        rootViewCoverAlphaForLeftView = 0.9
        
        leftViewPresentationStyle = .slideAbove
        // -----
        
    }
    
    override func leftViewWillLayoutSubviews(with size: CGSize) {
        super.leftViewWillLayoutSubviews(with: size)
        
        if !isLeftViewStatusBarHidden {
            leftView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
        }
    }
    
    override var isLeftViewStatusBarHidden: Bool {
        get {
            if (type == 8) {
                return UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation) && UI_USER_INTERFACE_IDIOM() == .phone
            }
            return super.isLeftViewStatusBarHidden
        }
        set {
            super.isLeftViewStatusBarHidden = newValue
        }
    }
    
    deinit {
        print("MainViewController deinitialized")
    }
    
}
