//
//  Date.swift
//  AmbrishDarshan
//
//  Created by MAC on 11/12/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import Foundation
extension Date {
    static func getFormattedDate(string: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz" // This formate is input formated .
        
        let formateDate = dateFormatter.date(from:string)!
        dateFormatter.dateFormat = "dd-MM-yyyy" // Output Formated
        
        return dateFormatter.string(from: formateDate)
    }
}
