//
//  APIType.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import Foundation
@objc enum APIType: Int {
    case api = 0
    case Login
    case Registration
    case Sms
    case UserList
    case CityList
    case UserDetails
    case Sugession
    case UserProfile
    case GroupList
    case AddGroupMember
    case AddDevotee
    
    case AddAdmin
    case GetAdmin
    case DeleteAdmin
    
    case CreateGroup
    case DeleteGroup
    case EditGroup
    
    case GuestRequest
    case GuestApprove
    case ApproveRequest
    case DeleteRequest
    case DeleteUser
    
    case SuggestionList
}
