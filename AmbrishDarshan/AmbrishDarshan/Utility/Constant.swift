//
//  Constant.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import Foundation
import UIKit

let BASE_URL                        = "http://hddevelopers.in/ambrishdarshannew/webservices/"
let URL_LOGIN                       = "\(BASE_URL)login/login.php"
let URL_REGISTRATION                = "\(BASE_URL)guest/guest.php"
let URL_SMS                         = "\(BASE_URL)login/sms.php"
let URL_USER_LIST                   = "\(BASE_URL)filter/ambrish_list.php"
let URL_CITY_LIST                   = "\(BASE_URL)showdata/city_list.php"
let URL_GETUSER_FULLDATA            = "\(BASE_URL)showdata/showdata.php"
let URL_SUGGESSION                  = "\(BASE_URL)contactus/contactus.php"
let URL_GET_USERPROFILE             = "\(BASE_URL)afterlogin/after_login_get_data.php"
let URL_GET_GROUPLIST               = "\(BASE_URL)group/group_list.php"

// ADMIN SIDE
let URL_ADDGROUP_MEMBER             = "\(BASE_URL)group/add_in_group.php?"
let URL_ADD_ADMIN                   = "\(BASE_URL)admin/add.php"
let URL_GET_ADMIN                   = "\(BASE_URL)admin/admin_list.php"
let URL_DELETE_ADMIN                = "\(BASE_URL)admin/delete.php?"
let URL_DELETE_USER                 = "\(BASE_URL)filter/delete.php?"

//Group
let URL_CREATE_GROUP                = "\(BASE_URL)group/add.php"
let URL_DELETE_GROUP                = "\(BASE_URL)group/delete_group.php?"
let URL_EDIT_GROUP                = "\(BASE_URL)group/update_group.php?"

//Approval
let URL_GUEST_REQUEST               = "\(BASE_URL)guest/request_list.php"
let URL_GUEST_APPROVE               = "\(BASE_URL)guest/guest_list.php"
let URL_APPROVE_REQUEST             = "\(BASE_URL)guest/request_update.php?"
let URL_DELETE_REQUEST              = "\(BASE_URL)guest/delete.php?"

//Suggetion
let URL_SUGGESTION_LIST             = "\(BASE_URL)contactus/suggestion_list.php"

let BORDER_COLOR                    = UIColor(red: 0/255, green: 191/255, blue: 239/255, alpha: 1.0)
let WHITE_COLOR                     = UIColor.white

// ADMIN
let URL_ADDDEVOTEE                      = "\(BASE_URL)ambrish_add_update/fileupload.php"

enum UIUserInterfaceIdiom : Int
{
    case unspecified
    case phone
    case pad
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}

