//
//  AppData.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import Foundation
import UIKit
class AppData {
    
    var storyboard : UIStoryboard!;
    var appDelegate: AppDelegate!;
    
    var OTP:NSNumber = NSNumber()
    var userDetailsDic:NSDictionary = NSDictionary()
    
    var userMobileNumber:String = String()
    var userProfileDic:NSDictionary = NSDictionary()
    static let sharedInstance: AppData = {
        let instance = AppData()
        
        // some additional setup
        return instance
    }()
    
}
