//
//  Utility.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.

import Foundation
import SVProgressHUD
import Reachability
import Alamofire
import UIKit
@objc protocol  UtilityDelegate {
    @objc optional func apiCallCompleted(_ success: Bool, data: NSArray?, error: String, apiType: APIType)
}
class Utility {
    
    var window: UIWindow?
    var delegate: UtilityDelegate?
    
    class func showAlert(_ title: String?, message: String?, viewController: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction) in
        }))
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    class func showAlertWithAction(_ title: String?, message: String?, viewController: UIViewController, actions:[UIAlertAction] = [UIAlertAction(title: "Ok", style: .cancel, handler: nil)]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        for action in actions {
            alert.addAction(action)
        }
        viewController.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Add shadow to View..
    class func addShadow(view: UIView) {
        let caDemoButtonLayer: CALayer? = view.layer
        caDemoButtonLayer?.frame = view.frame
        caDemoButtonLayer?.shadowRadius = 1.0
        caDemoButtonLayer?.cornerRadius = 10.0
        caDemoButtonLayer?.borderWidth = 2.0
        caDemoButtonLayer?.borderColor = BORDER_COLOR.cgColor
        caDemoButtonLayer?.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(1.0))
    }
    
    class func addImageShadow(image: UIImageView) {
        let caDemoButtonLayer: CALayer? = image.layer
        caDemoButtonLayer?.frame = image.frame
        caDemoButtonLayer?.shadowRadius = 1.0
        caDemoButtonLayer?.cornerRadius = 10.0
        caDemoButtonLayer?.borderWidth = 2.0
        caDemoButtonLayer?.borderColor = BORDER_COLOR.cgColor
        caDemoButtonLayer?.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(1.0))
    }
    
    class func buttonAddShadow(view: UIButton) {
        let caDemoButtonLayer: CALayer? = view.layer
        caDemoButtonLayer?.frame = view.frame
        caDemoButtonLayer?.shadowRadius = 1.0
        caDemoButtonLayer?.cornerRadius = 8.0
        //        caDemoButtonLayer?.shadowOpacity = 0.3
        caDemoButtonLayer?.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(1.0))
    }
    
    class func imageBorder(view:UIView){
        view.layer.cornerRadius = 10.0//self.imageview.frame.size.width / 2;
        view.layer.borderWidth = 1.5
        view.layer.borderColor = UIColor.black.cgColor
        view.clipsToBounds = true
    }
    
    class func imageViewBorder(image: UIImageView){
        image.layer.cornerRadius = 10.0
        image.layer.borderWidth = 1.5
        image.layer.borderColor = UIColor.black.cgColor
        image.clipsToBounds = true
    }
    
    class func showProgress(_ message: String) {
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        if(message == "") {
            SVProgressHUD.show()
        }
        else {
            SVProgressHUD.show(withStatus: message)
        }
    }
    
    class func dismissProgress() {
        SVProgressHUD.dismiss()
    }
    
    class func checkInternet() -> Bool {
        let networkReachability: Reachability = Reachability.forInternetConnection();
        let networkStatus : NetworkStatus = networkReachability.currentReachabilityStatus();
        if (networkStatus.rawValue == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    func UserLogin(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1{
                        let data:NSArray = result.value(forKey: "data") as! NSArray
                        self.delegate?.apiCallCompleted!(true, data: data, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func SendSMS(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let temp:NSDictionary = response.result.value as! NSDictionary
                    AppData.sharedInstance.OTP = temp.value(forKey: "otp") as! NSNumber
                    self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func UserProfileData(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1{
                        AppData.sharedInstance.userProfileDic = result
                        self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func User_Registration(request:URLRequest , imgData:Data , fileName:String ,  parameters:[String :String] , apiType:APIType){
        if Utility.checkInternet(){
            let strBase64 = imgData.base64EncodedString(options: .lineLength64Characters)
            
            Alamofire.upload(multipartFormData: { MultipartFormData in
                MultipartFormData.append(imgData, withName: "image_i", fileName: fileName, mimeType: "image/jpg")
                
                for (key, value) in parameters {
                    MultipartFormData.append((value.data(using: String.Encoding.utf8)!), withName: key)
                }
            },with: request,encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                    } case .failure( _):
                        self.delegate?.apiCallCompleted!(false, data: nil, error: "", apiType: apiType)
                }
            })
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func User_Sugession(request:URLRequest , parameters:[String :String] , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.upload(multipartFormData: { MultipartFormData in
                for (key, value) in parameters {
                    MultipartFormData.append((value.data(using: String.Encoding.utf8)!), withName: key)
                }
            },with: request,encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                    } case .failure( _):
                        self.delegate?.apiCallCompleted!(false, data: nil, error: "", apiType: apiType)
                }
            })
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func Get_UserList(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        let tempArray:NSArray = result.value(forKey: "data") as! NSArray
                        self.delegate?.apiCallCompleted!(true, data: tempArray, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func Get_GroupList(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        let tempArray:NSArray = result.value(forKey: "data") as! NSArray
                        self.delegate?.apiCallCompleted!(true, data: tempArray, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func Get_CityList(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1{
                        let data:NSArray = result.value(forKey: "data") as! NSArray
                        self.delegate?.apiCallCompleted!(true, data: data, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func Get_UserDetails(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1{
                        AppData.sharedInstance.userDetailsDic = result
                        self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func AddGroupMember(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        self.delegate?.apiCallCompleted!(true, data: nil, error: message, apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func AddDevotee(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        self.delegate?.apiCallCompleted!(true, data: nil, error: message, apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func AddAdmin(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                print(response.result)
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        self.delegate?.apiCallCompleted!(true, data: nil, error: message, apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    
    func GetAdminList(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1{
                        
                        let data:NSArray = result.value(forKey: "data") as! NSArray
                        self.delegate?.apiCallCompleted!(true, data: data, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func DeleteAdmin(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1{
                        self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func CreateGroup(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    print(result)
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        self.delegate?.apiCallCompleted!(true, data: nil, error: message, apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func DeleteGroup(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                print(response.result)
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1{
                        self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func EditGroup(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                print(response.result)
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1{
                        self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func Get_GuestRequestList(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        let tempArray:NSArray = result.value(forKey: "data") as! NSArray
                        self.delegate?.apiCallCompleted!(true, data: tempArray, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func Get_GuestApproveList(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        let tempArray:NSArray = result.value(forKey: "data") as! NSArray
                        self.delegate?.apiCallCompleted!(true, data: tempArray, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func ApproveRequest(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func DeleteRequest(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func Get_SuggestionList(request:URLRequest , apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseJSON { response in
                if response.error == nil {
                    let result:NSDictionary = response.result.value as! NSDictionary
                    let message = result.value(forKey: "message") as! String
                    if result.value(forKey: "success") as! NSNumber == 1 {
                        let tempArray:NSArray = result.value(forKey: "data") as! NSArray
                        self.delegate?.apiCallCompleted!(true, data: tempArray, error: "", apiType: apiType)
                    }
                    else{
                        self.delegate?.apiCallCompleted!(false, data: nil, error: message, apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: "\(response.error?.localizedDescription ?? "")", apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
    
    func AddDevotee(request:URLRequest , imgData:Data , fileName:String ,  parameters:[String :String] , apiType:APIType){
        if Utility.checkInternet(){
            let strBase64 = imgData.base64EncodedString(options: .lineLength64Characters)
            Alamofire.upload(multipartFormData: { MultipartFormData in
                MultipartFormData.append(imgData, withName: "image_i", fileName: fileName, mimeType: "image/jpg")
                
                for (key, value) in parameters {
                    MultipartFormData.append((value.data(using: String.Encoding.utf8)!), withName: key)
                }
            },with: request,encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        self.delegate?.apiCallCompleted!(true, data: nil, error: "", apiType: apiType)
                    } case .failure( _):
                        self.delegate?.apiCallCompleted!(false, data: nil, error: "", apiType: apiType)
                }
            })
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "please check your network connection and try again", apiType: apiType)
        }
    }
}
