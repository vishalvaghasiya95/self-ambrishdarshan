//
//  HomeTableViewCell.swift
//  AmbrishDarshan
//
//  Created by MAC on 12/10/18.
//  Copyright © 2018 vishal. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileImageTapButton: UIButton!
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_MobileNumber: UILabel!
    @IBOutlet weak var imageview: UIView!
    @IBOutlet weak var callButton: UIButton!
    
    @IBOutlet weak var selectCityButton: UIButton!
    @IBOutlet weak var selectCityImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
